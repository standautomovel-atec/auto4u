## O que é?

O projecto consiste em um website sobre um stand automóvel.

---
## Estrutura

/pages -> Contém os ficheiros html
/js -> Contém javascript dedicados
/vendor -> Contém os javascript externos
/imgs -> Contém as imagens do website
/css -> Contém os ficheiros de estilo css

---
## Como publicar 

Para publicar este projecto é necessário um servidor que contenha uma instalação de apache nginx ou iis o qual têm que estar configurado para interpretar ficheiros php.

---
