<!doctype html>
<html lang="pt">
    <head>
        <title>Quem Somos</title>
        <meta charset="utf-8"/>
        <?php 
            $pag= "q";
            $title="Auto4u | Quem Somos";
            include_once("layouts/header.php");
        ?>
    </head>
    <body>
        <section class="qsomos">
            <div>
                <h1>Quem Somos</h1>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <p>
                            <strong>A Auto4u é uma empresa com 20 anos de experiência automóvel.</strong>
                        </p>
                        <p>Desde cedo que a Auto4u é líder no mercado na venda de automóveis. Tudo graças a uma equipa de jovens profissionas competentes que asseguram a excelência dos nossos serviços.</p>
                        <p>O nosso conceito assenta em valores como honestidade, confiança e seriedade e prima pela transparencia na negociação e atendimento.</p>
                        <p>Somos uma empresa multimarca que comercializa viaturas de todos os segmentos com várias soluções que se adequam às suas necessidades. Apostamos na modernização e actualização. Somos exigentes na escolha das viaturas que comercializamos tendo sempre em conta o factor preço/qualidade. Oferecemos as melhores vantagens em crédito automóvel, com as taxas mais competitivas do mercado.</p>
                        <p>A nossa missão é responder aos desafios dos nossos clientes e acompanhá-los nas mais diversas etapas na compra de um veículo. </p>
                        <p>Na Auto4u trabalhamos diariamente com um único sentido: a satisfação total dos nossos clientes.</p>
                        <p>Temos o compromisso de desempenhar o nosso trabalho baseado em relações de ética e confiança.</p>
                        <p>Esperamos por si, em Matosinhos, na Avenida Comendador Ferreira de Matos, nº 306. Faça-nos uma visita.</p>
                        <p>
                            <strong>
                                <u>
                                    Auto4u - Garantia de Confiança
                                </u>
                            </strong>

                        </p>
                    </div>
                    <div class="col-sm-12">
                        <img src="https://www.lowage.pt/web1/zp/tpl1/template/img/empresa.gif" class="img-responsive" alt="foto"/>

                    </div>
                </div>
            </div>
        </section>
        <?php
        require_once("layouts/footer.php");
        ?>
    </body>
</html>