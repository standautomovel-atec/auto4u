<html lang="en">
<body>
    <?php 
        $title="Auto4u | Viaturas";
        $pag= "v";
        require_once("layouts/header.php");
    ?>
    <main>
        <div class="container-fluid viatura">
            <div class="row">
                <div class="col-md-12 viatura-nome">
                    <h1>Visualizar Viaturas</h1>
                </div>

                <div class="filtros col-md-12">
                    <section class="row menus">
                        <div class="col-sm-12 flexboys">
                            <div>
                                <select class="custom-select menu">
                                    <option selected>Marca</option>
                                    <option value="1">Citroen</option>
                                    <option value="2">Opel</option>
                                    <option value="3">Peugeot</option>
                                    <option value="4">Renault</option>
                                </select>
                            </div>
                            <div>
                                <select class="custom-select menu">
                                    <option selected>Tipo de Combustível</option>
                                    <option value="1">Gasoleo</option>
                                    <option value="2">Gasolina</option>
                                    <option value="3">GPL</option>
                                    <option value="4">Hibrido</option>
                                    <option value="5">Biofuel</option>
                                    <option value="6">Electrico</option>
                                    <option value="7">Hibrido Gasoleo</option>
                                    <option value="8">Hibrido Gasolina</option>
                                </select>
                            </div>
                            <div class="input-group menu">
                                <input type="text" style="margin-right:0%;" class="form-control menu" aria-label="Text input with segmented dropdown button" placeholder="1º Registo">
                            </div>

                            <div class="input-group ">
                                <input type="text" style="margin-right:0%;" class="form-control menu" aria-label="Text input with segmented dropdown button" placeholder="Preço máximo">
                            </div>

                            <div class="input-group ">
                                <input type="text" style="margin-right:0%; " class=" menu form-control" aria-label="Text input with segmented dropdown button" placeholder="Quilometros máximo">
                            </div>

                            <div class="input-group ">
                                <input type="text" style="margin-right:0%; " class=" menu form-control" aria-label="Text input with segmented dropdown button" placeholder="Número de lugares">
                            </div>

                        </div>

                        <div class="col-sm-4"></div>
                        <div class="col-sm-4 text-center">
                            <button type="button" class="btn btn-primary menu-btn"><span><i class="fas fa-search"></i></span></button>
                        </div>
                </div>

                <div class="col-md-12 relacionados">
                <section class="row">
                    <?php for ($i=0; $i < 9; $i++) { ?>
                    <article class="col-sm-4  list card">
                        <img src="../img/viatura/carocha.jpg" alt="card-img-top" alt="car">
                        <div class="card-body">
                            <h5 class="card-title">Example Car <span class="preco">10€</span></h5>
                            <p class="card-text text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                                            Veniam harum ad ullam accusamus commodi tenetur sapiente. 
                                            Excepturi fuga exercitationem ipsum!
                            </p>
                            <a href="viatura.php" class="card-link float-right">Mais...</a>
                        </div>
                    </article>
                    <?php } ?>
                </section> 

                </div>
            </div>        
        </div>
    </main>
    <?php
        require_once("layouts/footer.php");
    ?>
</body>
</html>