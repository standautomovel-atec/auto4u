<!DOCTYPE html>
<html>
    <?php 
        $title="Auto4u | Home";
        $pag= "i";
        require_once("layouts/header.php");
    ?> 
    <body>
        <section class="container-fluid">
            <section class="row">
                <div class="col-sm-12" style="padding:0%;">
                    <div class="swiper-container position-sticky">
                        <div class="swiper-wrapper">
                            <div class="col-sm-12 swiper-slide"><img class="img-fluid" src="../img/viatura/carocha.jpg" alt="carocha"></div>
                            <div class="col-sm-12 swiper-slide"><img src="../img/viatura/carocha.jpg" alt="carocha"></div>
                            <div class="col-sm-12 swiper-slide"><img src="../img/viatura/carocha.jpg" alt="carocha"></div>
                            <div class="col-sm-12 swiper-slide"><img src="../img/viatura/carocha.jpg" alt="carocha"></div>
                            <div class="col-sm-12 swiper-slide"><img src="../img/viatura/carocha.jpg" alt="carocha"></div>
                            <div class="col-sm-12 swiper-slide"><img src="../img/viatura/carocha.jpg" alt="carocha"></div>
                            <div class="col-sm-12 swiper-slide"><img src="../img/viatura/carocha.jpg" alt="carocha"></div>
                            <div class="col-sm-12 swiper-slide"><img src="../img/viatura/carocha.jpg" alt="carocha"></div>
                            <div class="col-sm-12 swiper-slide"><img src="../img/viatura/carocha.jpg" alt="carocha"></div>
                            <div class="col-sm-12 swiper-slide"><img src="../img/viatura/carocha.jpg" alt="carocha"></div>
                        </div>
                                <!-- Add Scrollbar -->
                        <div class="swiper-scrollbar"></div>
                    </div>
                </div>
                <section class="col-sm-12 card menu" style="margin-top:0%;">
                    <!--
                        Dropdown menu
                    -->
                    <section class="row menus">
                        <div class="col-sm-4 ">
                            <select class="custom-select menu">
                                <option selected>Marca</option>
                                <option value="1">Citroen</option>
                                <option value="2">Opel</option>
                                <option value="3">Peugeot</option>
                                <option value="4">Renault</option>
                            </select>
                        </div>

                        <div class="col-sm-4 ">
                            <select class="custom-select menu">
                                <option selected>Modelo</option>
                                <option value="1">Berlingo</option>
                                <option value="2">C2</option>
                                <option value="3">C3</option>
                                <option value="4">C4</option>
                                <option value="5">Jumper</option>
                            </select>
                        </div>

                        <div class="col-sm-4 input-group menu">
                            <div class="input-group-append ">
                                <button type="button" style="margin-left:0%; box-shadow:none;  background-color:dodgerblue;" class=" menu btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="sr-only"></span></button>
                                <div class="dropdown-menu">
                                    <a class='dropdown-item' href='#'>2001</a>
                                    <a class='dropdown-item' href='#'>2002</a>
                                    <a class='dropdown-item' href='#'>2003</a>
                                    <a class='dropdown-item' href='#'>2004</a>
                                    <a class='dropdown-item' href='#'>2005</a>
                                    <a class='dropdown-item' href='#'>2006</a>
                                </div>
                            </div>
                            <input type="text" style="margin-right:0%;" class="form-control menu" aria-label="Text input with segmented dropdown button" placeholder="1º Registo">
                        </div>

                        <div class="col-sm-4 input-group ">
                            <div class="input-group-append">
                                <button type="button" style="margin-left:0%; box-shadow:none;  background-color:dodgerblue;" class=" menu btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="sr-only"></span></button>
                                <div class="dropdown-menu">
                                    <a class='dropdown-item' href='#'>2,000Km</a>
                                    <a class='dropdown-item' href='#'>6,000Km</a>
                                    <a class='dropdown-item' href='#'>10,000Km</a>
                                    <a class='dropdown-item' href='#'>100,000Km</a>
                                    <a class='dropdown-item' href='#'>150,000Km</a>
                                    <a class='dropdown-item' href='#'>200,000Km</a>
                                </div>
                            </div>
                            <input type="text" style="margin-right:0%;" class="form-control menu" aria-label="Text input with segmented dropdown button" placeholder="Preço até">
                        </div>

                        <div class="col-sm-4 input-group ">
                            <div class="input-group-append">
                                <button type="button" style="margin-left:0%; box-shadow:none; background-color:dodgerblue;" class="menu btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="sr-only"></span></button>
                                <div class="dropdown-menu">
                                    <a class='dropdown-item' href='#'>2,000Km</a>
                                    <a class='dropdown-item' href='#'>6,000Km</a>
                                    <a class='dropdown-item' href='#'>10,000Km</a>
                                    <a class='dropdown-item' href='#'>100,000Km</a>
                                    <a class='dropdown-item' href='#'>150,000Km</a>
                                    <a class='dropdown-item' href='#'>200,000Km</a>
                                </div>
                            </div>
                            <input type="text" style="margin-right:0%; " class=" menu form-control" aria-label="Text input with segmented dropdown button" placeholder="Quilometros até">
                        </div>

                        <div class="col-sm-4 ">
                            <select class="custom-select menu">
                                <option selected>Combustível</option>
                                <option value="1">Gasoleo</option>
                                <option value="2">Gasolina</option>
                                <option value="3">GPL</option>
                                <option value="4">Hibrido</option>
                                <option value="5">Biofuel</option>
                                <option value="6">Electrico</option>
                                <option value="7">Hibrido Gasoleo</option>
                                <option value="8">Hibrido Gasolina</option>
                            </select>
                        </div>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4 text-center">
                            <button type="button" class="btn btn-primary menu-btn"><span><i class="fas fa-search"></i> Pesquisar</span></button>
                        </div>
                        
                        <div class="col-sm-4"></div>
                    </section>
                </section>
            </section>
        </section>
        <section class="container">
            <section class="row">
                <?php for ($i=0; $i < 9; $i++) { ?>
                    <article class="col-sm-4 list card">
                        <img src="../img/viatura/carocha.jpg" alt="card-img-top" alt="car">
                        <div class="card-body">
                            <h5 class="card-title">Example Car <span class="preco">10€</span></h5>
                            <p class="card-text text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                                            Veniam harum ad ullam accusamus commodi tenetur sapiente. 
                                            Excepturi fuga exercitationem ipsum!
                            </p>
                            <a href="../pages/viatura.php" class="card-link float-right">Mais...</a>
                        </div>
                    </article>
                <?php } ?>
            </section>
        </section>
        <?php
            require_once("layouts/footer.php");
        ?>
    </body>
</html>