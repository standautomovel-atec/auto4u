<html lang="en">
<body>
    <?php 
        $title="Auto4u | Viatura XPTO";
        $pag= "v";
        require_once("layouts/header.php");
    ?>
    <main>
        <div class="container-fluid viatura">
            <div class="row">
                <div class="col-md-12 viatura-nome">
                    <h1>Viatura XPTO</h1>
                </div>
                <div class="col-md-6 col-xs-12 viatura-foto">
                        <div class="swiper-container gallery-top">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                            </div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                            </div>
                            <div class="swiper-container gallery-thumbs">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('../img/viatura/carocha.jpg')"></div>
                            </div>
                        </div>                            
                </div>
                <div class="col-md-6">
                    <div class="col-md-5 col-sm-12 preco">
                        <a id="fav" onclick="fav('1')"></a>
                        10 € 
                    </div>
                    <div class="col-md-7 col-sm-12 botoes">
                        
                        <a class="btn reservar" href="#">Reservar</a>
                        <a class="btn contacto" href="#contactar">Entrar em contacto</a>
                    </div>
                    <table class="table caracteristicas">
                        <tr>
                            <td>Quilometragem</td>
                            <td>2 e meio KMs</td>
                        </tr>
                        <tr>
                            <td>Cilindrada/Potência</td>
                            <td>1968/150</td>
                        </tr>
                        <tr>
                            <td>Tipo de Combustível</td>
                            <td>Gasóleo</td>
                        </tr>
                        <tr>
                            <td>Cor Exterior</td>
                            <td>Azul</td>
                        </tr>
                        <tr>
                            <td>Data 1ª Matrícula</td>
                            <td>2016-05</td>
                        </tr>
                        <tr>
                            <td>Nº de Lugares</td>
                            <td>5</td>
                        </tr>
                        <tr>
                            <td>Nº de Portas</td>
                            <td>5</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-12 spacer"></div>
                <div class="col-md-8 principais">
                    <ul class="col-md-4">
                        <li>ABS</li>
                        <li>Bluetooth</li>
                        <li>Computador de Bordo</li>
                        <li>Cruise Control</li>
                        <li>Imobilizador</li>
                        <li>Livro de revisões completo</li>
                    </ul>
                    <ul class="col-md-4">
                        <li>Direcção Assistida</li>
                        <li>GPS</li>
                        <li>ISOFIX</li>
                        <li>Sensores Estacionamento</li>
                        <li>Ajuda ao parqueamento</li>
                    </ul>
                    <ul class="col-md-4">
                        <li>Alarme</li>
                        <li>Alerta sobre Manutenção</li>
                        <li>Alertas sobre Cinto de Segurança</li>
                        <li>EDS Bloqueio Electrónico do Diferencial</li>
                        <li>ESP</li>
                        <li>Sistema de Chave Inteligente</li>
                    </ul>
                </div>
                <div class="col-md-4 contactar">
                    <form>
                        <h2 id="contactar">Contacto</h2>
                        <input class="form-control" type="text" placeholder="Email">
                        <button type="submit" value="Submit" class="btn btn-confirm">Entrar em contacto</button>
                    </form>
                </div>
                <div class="col-md-12 spacer"></div>
                <div class="col-md-12 relacionados">
                <section class="row">
                    <div class="col-sm-12 titulo"><h1>Carros Relacionados</h1></div>
                    <?php for ($i=0; $i < 9; $i++) { ?>
                    <article class="col-sm-4  list card">
                        <img src="../img/viatura/carocha.jpg" alt="card-img-top" alt="car">
                        <div class="card-body">
                            <h5 class="card-title">Example Car <span class="preco">10€</span></h5>
                            <p class="card-text text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                                            Veniam harum ad ullam accusamus commodi tenetur sapiente. 
                                            Excepturi fuga exercitationem ipsum!
                            </p>
                            <a href="#" class="card-link float-right">Mais...</a>
                        </div>
                    </article>
                    <?php } ?>
                </section> 

                </div>
            </div>        
        </div>
    </main>
    <?php
        require_once("layouts/footer.php");
    ?>
</body>
</html>