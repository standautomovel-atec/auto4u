<?php
  require_once("head.php");
?>
<nav class="header">
  <div class="nav-wrapper" style="background-color:#0d47a1">
    <a href="../index.php" class="brand-logo">Auto4U</a>
    <form style="float:left;margin-left:10%;width: 35%;";> <!--Css Inline by Proprio -->
      <div class="input-field">
          <input id="search" type="search" required>
          <label class="label-icon" for="search"><i class="material-icons">search</i></label>
          <i class="material-icons">close</i>
      </div>
    </form>
    <a href="#" data-target="mobile-demo" style="margin-left:25%"  class="sidenav-trigger"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li><a href="../pages/index.php"><i class="material-icons left">home</i>Home</a></li>
      <li><a href="../pages/QuemSomos.php"><i class="material-icons left">people</i>Quem Somos</a></li>
      <li><a href="../pages/viaturas.php"><i class="material-icons left">directions_car</i>Viaturas</a></li>
      <li><a href="../pages/contactos.php"><i class="material-icons left">contact_phone</i>Contactos</a></li>
    </ul>
  </div>
</nav>
<ul class="sidenav" id="mobile-demo">
  <li><a href="../pages/index.php">Home</a></li>
  <li><a href="../pages/QuemSomos.php">Quem Somos</a></li>
  <li><a href="../pages/viaturas.php">Viaturas</a></li>
  <li><a href="../pages/contactos.php">Contactos</a></li>
</ul>
<div class="fixed-action-btn">
  <a class="btn-floating btn-large blue darken-4">
    <i class="large material-icons">mode_edit</i>
  </a>
  <ul id="nav-mobile" class="right hide-on-med-and-down">
    <li><a class="btn-floating btn-medium blue darken-4" href="../pages/index.php"><i class="material-icons">home</i>Inicio</a></li>
    <li><a class="btn-floating btn-medium blue darken-4" href="../pages/QuemSomos.php"><i class="material-icons">people</i></a></li>
    <li><a class="btn-floating btn-medium blue darken-4" href="../pages/viaturas.php"><i class="material-icons">directions_car</i></a></li>
    <li><a class="btn-floating btn-medium blue darken-4" href="../pages/contactos.php"><i class="material-icons">contacts</i></a></li>
    <li><a class="btn-floating btn-medium blue darken-4 pulse" href="../pages/contactos.php#contact"><i class="material-icons">call</i></a></li>                
  </ul>
</div>
        