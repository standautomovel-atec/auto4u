<head>
        <meta charset="utf-8" />
        <title>
        <?php 
        echo $title;
        ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <!-- ETC -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        


        <!-- CSS Vendor -->

        <link type="text/css" rel="stylesheet" href="../../css/materialize.css"  media="screen,projection"/>

        <link rel="stylesheet" href="../vendor/swiper/css/swiper.min.css">



        <link rel="stylesheet" href="../vendor/bootstrap/css/bootstrap.min.css"/>
        <script src="../vendor/jquery/jquery.min.js"></script> 
        <script src="../vendor/popper/popper.min.js"></script>
        <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
        
        <!-- JS Vendor-->
        <!-- <script src="../../vendor/swiper/js/swiper.js"></script> -->
        <!-- <script src="../../vendor/Bootstrap/js/bootstrap.js"></script> -->
        <!-- <script src="../../vendor/Bootstrap/js/bootstrap.bundle.js"></script> -->
        <!-- <script src="../../vendor/Bootstrap/js/bootstrap.bundle.min.js"></script> -->
        <script type="text/javascript" src="../js/materialize.js"></script>
        <!-- <script src="../../vendor/Bootstrap/js/bootstrap.min.js"></script> -->
        <script src="../vendor/swiper/js/swiper.min.js"></script>
        <!-- <script src="../vendor/viatura/swiper/js/swiper.min.js"></script> -->
        
        
        
        
        <!-- CSS Homemade-->
        <link type="text/css" rel="stylesheet" href="../../css/geral.css"/>
        
        
        
        <!-- JS Homemade -->
        
        
        <!-- <link rel="stylesheet" href="../vendor/viatura/swiper/css/swiper.min.css"/> -->
        <script src="../vendor/viatura/jscookies/jscookies.js"></script>
        



        <?php 
            if(isset($pag)) {
                if ($pag == "v"){
                    echo "<script src='../js/viatura/viatura.js'></script><link rel='stylesheet' href='../css/viatura.css'/>";
                }elseif($pag == "i"){
                    echo "<script src='../vendor/jquery/jquery.min.js'></script><script src='../vendor/popper/popper.min.js'></script><link rel='stylesheet' href='../vendor/bootstrap/css/bootstrap.min.css'/><script src='../vendor/bootstrap/js/bootstrap.min.js'></script><script src='../js/Index/index.js'></script><link rel='stylesheet' href='../css/index.css'>";
                }
                elseif($pag == "t"){
                    echo "<link rel='stylesheet' href='../css/contactos.css'>";
                }
                elseif($pag == "q"){
                    echo "<link rel='stylesheet' href='../css/quemsomos.css'>";
                }
            }
        ?>
    </head>    
        
    
    