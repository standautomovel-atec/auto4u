<!doctype html>
<html lang="pt">
    <head>
        <title></title>
        <meta charset="utf-8"/>
        <?php 
            $pag= "t";
            $title="Auto4u | Contactos";
            include_once("layouts/header.php");
        ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body>
        <section class="cont">
            <div>
            <h1>Contactos</h1>
                <section class="main-inform">
                    <section class="info" id="contact">
                        <div>
                        <p><span style="font-size: 20px; color: #4969db;">Morada:</span></p>
                        <p><span style="font-size:18px">Avenida Comendador Ferreira de Matos, nº 306</span></p>
                        <p><span style="font-size:18px">4450-120 Matosinhos</span></p>
                        <p><span style="font-size:20px; color: #4969db;">Contactos:</span></p>
                        <p><span style="font-size:18px">TLF +351 224 567 888</span></p>
                        <p><span style="font-size:18px">TLM +351 224 567 899</span></p>
                        <p><span style="font-size:18px">Email: auto4u@auto4u.com</span></p>
                        <p><span style="font-size:20px; color: #4969db;">Horário:</span></p>
                        <p><span style="font-size:18px">Segunda a Sexta das 09:00h às 20:00h</span></p>
                        <p><span style="font-size:18px">Sábado das 09:00h às 19:00h</span></p>
                        <p><span style="font-size: 18px">Domingo das 10:00h às 18:00h</span></p>
                        <p><span style="font-size: 18px">(intervalo de almoço 13:00h às 14:30h)</span></p>
                        </div>
                    </section>
                    <section class="contac">    
                        <section class="formulario">
                            <form action="#" method="POST">
                            <input name="nome" type="text" placeholder="Nome"/>
                            <input name="telefone" type="text" placeholder="Telefone"/>
                            <input name="email" type="email" placeholder="Email"/>
                            <input name="assunto" type="text" placeholder="Assunto"/>
                            <textarea placeholder="Mensagem"></textarea>
                            <p class="botao">
                                <input type="submit" value="Enviar"/>
                            </p>
                            </form>
                        </section>
                    </section>
                </section>
                <section class="map">
                    <div class="mapouter"><div><iframe id="gmap_canvas" src="https://maps.google.com/maps?q=matosinhos&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div></div>
                </section>
            </div>
        </section>
        <?php
            require_once("layouts/footer.php");
        ?>
    </body>
</html>