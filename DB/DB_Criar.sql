create database Auto4u
go
use Auto4u
go
CREATE TABLE Cor(nome varchar(40) primary key)
go
CREATE TABLE nrlugares(lugares int primary key)
go
CREATE TABLE nrportas(portas int primary key)
go
CREATE TABLE marca(nome varchar(50) primary key)
go
CREATE TABLE modelo(nome varchar(70) primary key, marca varchar(50))
go
ALTER TABLE modelo ADD FOREIGN KEY (marca) REFERENCES marca(nome)
go
CREATE TABLE tipo_combustivel(tipo varchar(50) primary key)
go
CREATE TABLE Veiculo (ID_Veiculo int primary key, 
						KMs int, 
						potencia int,
						preco int, 
						datareg date, 
						cilindrada int, 
						desconto int, 
						modelo varchar(70), 
						tipo_combustivel varchar(50), 
						cor varchar(40), 
						nrlugares int, 
						desconto_ativo bit, 
						nrportas int,
						categoria text,
						descricao text)
go
ALTER TABLE Veiculo ADD FOREIGN KEY (modelo) REFERENCES modelo(nome)
ALTER TABLE Veiculo ADD FOREIGN KEY (tipo_combustivel) REFERENCES tipo_combustivel(tipo)
ALTER TABLE Veiculo ADD FOREIGN KEY (cor) REFERENCES Cor(nome)
ALTER TABLE Veiculo ADD FOREIGN KEY (nrlugares) REFERENCES nrlugares(lugares)
ALTER TABLE Veiculo ADD FOREIGN KEY (nrportas) REFERENCES nrportas(portas)
go
CREATE TABLE Extra (nome varchar(100) primary key)
go
CREATE TABLE Extra_Carro (Veiculo int NOT NULL, extra varchar(100) NOT NULL)
go
ALTER TABLE Extra_Carro ADD PRIMARY KEY (Veiculo,extra)
ALTER TABLE Extra_Carro ADD FOREIGN KEY (Veiculo) REFERENCES Veiculo(ID_Veiculo)
ALTER TABLE Extra_Carro ADD FOREIGN KEY (extra) REFERENCES Extra(nome)
go
CREATE TABLE imgs(foto varchar(200) primary key, veiculo int)
go
ALTER TABLE imgs ADD FOREIGN KEY (veiculo) REFERENCES Veiculo(ID_Veiculo)